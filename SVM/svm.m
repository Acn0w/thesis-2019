% Author: Alessandro Cecchin (aka Acnow)
% Created: September 2019


%run in terminal matlab -nosplash -nodisplay -r "svm"
clc
clear all

addpath('../mfcc_dwt/functions/');
addpath('libsvm_3.24/');

%this is just a test set in order to debug code

folder1 = dir('../../test/');
folder2 = dir('../../test/');
folder1 = folder1(3:end);
folder2 = folder2(3:end);

train1 = [];
train2 = [];
test1 = [];
test2 = [];

for i=1:floor(size(folder1,1)/2)
    %half samples in train and the other in test
    [segnale1, Fs1] = audioread(strcat(folder1(1).folder,'/',folder1(2*i).name));
    %if the number of elements is even, then I skip the last odd one
    if(not(i*2==size(folder1,1) && rem(size(folder1,1),2)==0))
        [segnale2, Fs2] = audioread(strcat(folder1(1).folder,'/',folder1(2*i+1).name));
        %tmp_test1 = mfcc(segnale2,Fs2);
        
        %decomposition levels
        dec_lv = 7;
        %round(log2(size(segnale2,1))/2);
        %downscaling for the number of channels in get_dwt.m
        down_sc = (2^(dec_lv+2))/12;
        tmp_test1 = get_dwt(segnale2(:,1),2^(dec_lv+2),round(size(segnale2,1)/down_sc),dec_lv,'matlab');
        tmp_test1 = tmp_test1(:);
        %test1 = [test1 ; tmp_test1(:)];
        %remove infinites
        tmp_test1(tmp_test1==-Inf) = 0;
        tmp_test1(tmp_test1==+Inf) = 0;
        tmp_test1(:,1) = normalize(tmp_test1(:,1));
        format_data_lbsvm(tmp_test1,'-1','test1');
    end
    %tmp_train1 = mfcc(segnale1,Fs1);
    %decomposition levels
    dec_lv = 7;
    %round(log2(size(segnale1,1))/2);
    %downscaling for the number of channels in get_dwt.m
    down_sc = (2^(dec_lv+2))/12;
    tmp_train1 = get_dwt(segnale1(:,1),2^(dec_lv+2),round(size(segnale1,1)/down_sc),dec_lv,'matlab');
    tmp_train1 = tmp_train1(:);
    %train1 = [train1 ; tmp_train1(:)];
    %remove infinites
    tmp_train1(tmp_train1==-Inf) = 0;
    tmp_train1(tmp_train1==+Inf) = 0;
    tmp_train1(:,1) = normalize(tmp_train1(:,1));
    format_data_lbsvm(tmp_train1,'-1','train1');
    
end

for i=1:floor(size(folder2,1)/2)
    %half samples in train and the other in test
    [segnale1, Fs1] = audioread(strcat(folder2(1).folder,'/',folder2(2*i).name));

    if(not(i*2==size(folder2,1) && rem(size(folder2,1),2)==0))
        [segnale2, Fs2] = audioread(strcat(folder2(1).folder,'/',folder2(2*i+1).name));
        %tmp_test2 = mfcc(segnale2,Fs2);
        
        %decomposition levels
        dec_lv = 7;
        %round(log2(size(segnale2,1))/2);
        %downscaling for the number of channels in get_dwt.m
        down_sc = (2^(dec_lv+2))/12;
        tmp_test2 = get_dwt(segnale2(:,1),2^(dec_lv+2),round(size(segnale2,1)/down_sc),dec_lv,'matlab');
        tmp_test2 = tmp_test2(:);
        %test2 = [test2 ; tmp_test2(:)];
        tmp_test2(tmp_test2==-Inf) = 0;
        tmp_test2(tmp_test2==+Inf) = 0;
        tmp_test2(:,1) = normalize(tmp_test2(:,1));
        format_data_lbsvm(tmp_test2,'+1','test2');
    end
    %tmp_train2 = mfcc(segnale1,Fs1);
    
    %decomposition levels
    dec_lv = 7;
    %round(log2(size(segnale1,1))/2);
    %downscaling for the number of channels in get_dwt.m
    down_sc = (2^(dec_lv+2))/12;
    tmp_train2 = get_dwt(segnale1(:,1),2^(dec_lv+2),round(size(segnale1,1)/down_sc),dec_lv,'matlab');
    tmp_train2 = tmp_train2(:);
    %train2 = [train2 ; tmp_train2(:)];
    tmp_train2(tmp_train2==-Inf) = 0;
    tmp_train2(tmp_train2==+Inf) = 0;
    tmp_train2(:,1) = normalize(tmp_train2(:,1));
    format_data_lbsvm(tmp_train2,'+1','train2');

end

%------------------------------------------------------------------------------------------------
[label_mfcc, data_mfcc] = libsvmread('../mfcc_dwt/dataset_libsvm_mfcc.txt');
[label_dwt, data_dwt] = libsvmread('../mfcc_dwt/dataset_libsvm_dwt.txt');

%create the dataset MFCC in order to train the SVM
size_train = round(size(data_mfcc,1)*0.8);
Xtrain_mfcc = data_mfcc(1:size_train,:);
Xtest_mfcc = data_mfcc(size_train:end,:);
Ytest_mfcc = label_mfcc(size_train:end,:);
Ytrain_mfcc = label_mfcc(1:size_train,:);

%create dataset DWT
Xtrain_dwt = data_dwt(1:size_train,:);
Xtest_dwt = data_dwt(size_train:end,:);
Ytest_dwt = label_dwt(size_train:end,:);
Ytrain_dwt = label_dwt(1:size_train,:);

tic

%Accuracy dwt 91.5842% with '-h 0 -t 2 -g 0.05 -c 100'
%Accuracy MFCC baseline 89.604% with '-h 0 -t 2 -g 0.01 -c 1000' 
model_mfcc = svmtrain(Ytrain_mfcc, Xtrain_mfcc, '-h 0 -t 2 -g 0.01 -c 1000');
model_dwt = svmtrain(Ytrain_dwt, Xtrain_dwt, '-h 0 -t 2 -g 0.05 -c 100');


[predicted_label_mfcc, accuracy_mfcc, decision_values_mfcc] = svmpredict(Ytest_mfcc, Xtest_mfcc, model_mfcc);
[predicted_label_dwt, accuracy_dwt, decision_values_dwt] = svmpredict(Ytest_dwt, Xtest_dwt, model_dwt);

toc

save('model_mfcc','model_mfcc');
save('model_dwt','model_dwt');
save('decision_values_mfcc','decision_values_mfcc');
save('decision_values_dwt','decision_values_dwt');

%------------------------------------------------------------------------------------------------

%just merge all into the same matrix and sum the two columns with each other
labels_tmp = [Ytest_dwt, Ytest_dwt];
tl_tot_real = round(sum(labels_tmp(:,[1 2]),2));
clear labels_tmp;

%load vector of decision values
dv_mfcc = load("decision_values_mfcc.mat");
dv_mfcc = dv_mfcc.decision_values_mfcc;
dv_dwt = load("decision_values_dwt.mat");
dv_dwt = dv_dwt.decision_values_dwt;


%just merge all into the same matrix and sum the two columns with each other
labels_tmp = [dv_dwt,dv_mfcc];
labels_tot_pred = round(sum(labels_tmp(:,[1 2]),2));

%clear all unused var, just for the sake of RAM
clear labels_tmp;
clear dv_dwt;
clear dv_mfcc;

% I have to take the sign() of all decision values, then compare it with the 
% real labels, and finally calculate the accuracy of the two SVMs merged

labels_tot_pred = sign(labels_tot_pred);


%comnined accuracy 
accuracy_tot = sum(tl_tot_real(:,1)==labels_tot_pred(:,1))/size(tl_tot_real,1)*100;

