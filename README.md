# ANALYSIS OF SIGNALS EXTRACTED FROM A SMARTWATCH <br> FOR THE RECOGNITION OF MICROSLEEP EVENTS 
<br>
Thanks to the collaboration with Oraigo SRL, in this thesis will be discused a new approach <br>
for the recognition of events of a particular descriptor, starting from signals extracted <br>
from a biometric sensor.<br>
This electronic device can record a large array of signals in real time.<br>
In this report, it'll be studied a generic signal and its elaboration and organization into structures.<br>
In the first part, it will be presented the method used for the feature extraction <br>
of signals collected.<br>
In the second part, it'll be analyzed the implementation of a Support Vector Machine <br>
for classification into two categorie: event occured or not.<br>
In the third part, it'll be examinated how a Long Short Term Memory model works<br>
and how tune the iperparameters in order to identify the events with a great degree of accuracy.