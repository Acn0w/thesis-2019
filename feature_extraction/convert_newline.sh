#Author: Alessandro Cecchin AKA Acn0w
#Used in order to convert the csv files producted by the matlab code.
#Replace ; with \n in tmp_* and create the new files dataset_*
#!/bin/bash
tr ';' '\n' < tmp_mfcc.txt > dataset_mfcc.txt;
tr ';' '\n' < tmp_dwt.txt > dataset_dwt.txt;
