% Author: Alessandro Cecchin (aka Acnow)
% Created: September 2019


function elaborate(matr,Fs,label)
% label can be 0 or 1 (event occured)
mel_feat_extr = [];
dwt_feat = [];
matr_end = [];

    % Split the original vector into chunks of 10k elements and elaborate each chunk 
    % separately -> less RAM used.
    % I organize all chunks into a matrix
    if (size(matr,1)>100100)
        matr_end = matr((floor(size(matr,1)/100000)*100000)+1: end);
        if (size(matr_end,1)<70000)
            matr_end = matr(size(matr,1)-size(matr_end,1)-99999:end);
            matr = matr(1:size(matr,1)-size(matr_end,1));
            matr = reshape(matr,100000,[]);
        else
        matr = matr(1:size(matr,1)-size(matr_end,1));
        matr = reshape(matr,100000,[]);
        end
    end
    % Till the matrix have rows it continue the extraction, then I put
    % together all the pieces
    for i = 1:size(matr,2)

        if(Fs<1200)
            tmp = mfcc(matr(:,i), Fs*34);
            %I reshape the matrix as a column vetor and add every piece to it.
            tmp = tmp(:);
            mel_feat_extr = [mel_feat_extr ; tmp];
            %decomposition levels
            dec_lv = round(log2(size(matr,1))/2);
            %downscaling for the number of channels in get_dwt.m
            down_sc = (2^(dec_lv+2))/12;
            tmp = get_dwt(matr(:,i),2^(dec_lv+2),round(size(matr,1)/down_sc),dec_lv,'matlab');

            tmp = tmp(:);
            dwt_feat = [dwt_feat ; tmp];
        else
            tmp = mfcc(matr(:,i), Fs);
            tmp = tmp(:);
            mel_feat_extr = [mel_feat_extr ; tmp];
            %decomposition levels
            dec_lv = round(log2(size(matr,1))/2);
            %downscaling for the number of channels in get_dwt.m
            down_sc = (2^(dec_lv+2))/12;
            tmp = get_dwt(matr(:,i),2^(dec_lv+2),round(size(matr,1)/down_sc),dec_lv,'matlab');

            tmp = tmp(:);
            dwt_feat = [dwt_feat ; tmp];
        end


    end

    if(not(isempty(matr_end)))
    %We add the last piece of the matrix, if it exist
    if(Fs<1200)
        tmp = mfcc(matr_end, Fs*34);
        tmp = tmp(:);
        mel_feat_extr = [mel_feat_extr ; tmp];
        %decomposition levels
        dec_lv = round(log2(size(matr_end,1))/2);
        %downscaling for the number of channels in get_dwt.m
        down_sc = (2^(dec_lv+2))/12;
        tmp = get_dwt(matr_end,2^(dec_lv+2),round(size(matr_end,1)/down_sc),dec_lv,'matlab');

        tmp = tmp(:);
        dwt_feat = [dwt_feat ; tmp];
    else
        tmp = mfcc(matr_end, Fs);
        tmp = tmp(:);
        mel_feat_extr = [mel_feat_extr ; tmp];
        %decomposition levels
        dec_lv = round(log2(size(matr_end,1))/2);
        %downscaling for the number of channels in get_dwt.m
        down_sc = (2^(dec_lv+2))/12;
        tmp = get_dwt(matr_end,2^(dec_lv+2),round(size(matr_end,1)/down_sc),dec_lv,'matlab');
        tmp = tmp(:);
        dwt_feat = [dwt_feat ; tmp];
    end

    end

%I normalize the values and remove all Infinites (-> 0)
mel_feat_extr(mel_feat_extr==-Inf) = 0;
mel_feat_extr(mel_feat_extr==+Inf) = 0;
mel_feat_extr(:,1) = normalize(mel_feat_extr(:,1));

dwt_feat(dwt_feat==-Inf) = 0;
dwt_feat(dwt_feat==+Inf) = 0;
dwt_feat(:,1) = normalize(dwt_feat(:,1));

%format the data with the libsvm format, but instead of '\n' I use ';'
%since for some strange reasons matlab doesn't encode correctly that.

format_data_lbsvm(mel_feat_extr(:,1),label,"tmp_mfcc");
format_data_lbsvm(dwt_feat(:,1),label,"tmp_dwt");

