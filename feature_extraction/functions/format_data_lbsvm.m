% Author: Alessandro Cecchin (aka Acnow)
% Created: September 2019

function format_data_lbsvm(x,label,name)
% we need to convert to: 'label' 'class 1':'value1 2':'value2' ...
% The “ strcat ” function ignores trailing whitespace characters in 
% character vectors. However, “strcat” preserves them in cell arrays of 
% character vectors or string arrays.
n_col=size(x,1);
s=[];
filename = strcat(name,'.txt');
fid = fopen(filename, 'a');

for j=1:n_col
    %I need to remove zero indexes 
    if (j==1)
        s = label+" "+num2str(j)+':'+num2str(x(j,1));
    end
    
    if (not(x(j,1)==0) && not(j==1))
        s = s +" "+num2str(j)+':'+num2str(x(j,1));
    end
    
end    

fprintf(fid,'%s ;', s);


    
fclose(fid);
