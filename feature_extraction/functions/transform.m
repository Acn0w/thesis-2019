% Author: Alessandro Cecchin (aka Acnow)
% Created: September 2019


function  transform(matr,init,n_ev,time)
Fs = 64;
% sampling frequency of 64Hz

if (n_ev==0)

     elaborate(matr, Fs, -1);
    
else
    %I determine the number of cells that between the sampling start and the first event
    %Every interval is elaborated separetly and it will inserted into a 
    %feature vector with a specific label.
    %Event occured -> label 1, otherwise 0.
    
    tmp = 1;
    
    for i=1:n_ev
        num_cells = (round(time(i,1))-round(init))/0.015625;
        if(i==1)
            matr_part = matr(tmp:num_cells);
        else
            matr_part = matr(tmp:num_cells+tmp);
        end
            
         elaborate(matr_part,Fs,-1);

        %find the cells interval described into time.txt 
        tmp = num_cells+1;
        num_cells = (round(time(i,2))-round(time(i,1)))/0.015625;
        matr_part = matr(tmp:num_cells+tmp);
        elaborate(matr_part,Fs,1);
        
        %put the starting event time into init, and I use it for the next
        %runs. So every time I have a placeholder with the start of the
        %event and the finish (in time.txt)
        init = time(i,2);
        tmp = num_cells + 1;
        
    end
end
