% Author: Alessandro Cecchin (aka Acnow)
% Created: September 2019


clc
clear all
% path for the functions
addpath('functions/');

%We use a structure like this
% user1
%    |-> round1 
%            |-> .csv
%    |-> round2
%            |-> .csv
%    |-> round3
%            |-> .csv
% user2
%    |-> round1
%           |-> .csv
%    |
%    ...
%And now we choose the starting directory with all users.
path = uigetdir;

% take the list of all folders of each users
folders = dir(path);
%remove the starting dirs '.' and '..'
folders = folders(3:end);


for i=1:length(folders)
    %compose the path of subdir
    pathSes = strcat(path,'/',folders(i).name);
    sessione = dir(pathSes);
    sessione = sessione(4:end);
    sessione = sessione(1:(end-2));
    %Read the files with timestamp and number of events that occured 
    num_ev = fscanf(fopen(strcat(pathSes,'/','num_events.txt'),'r'),'%d');
    time = fscanf(fopen(strcat(pathSes,'/','time.txt'),'r'),'%f %f',[2 Inf]).';
    
    
    %real loop for the feature extraction
    for j=1:length(sessione)
        %load signal.csv.
        matr = readmatrix(strcat(pathSes,'/',sessione(j).name,'/signal.csv'));
        %take the starting time of sampling 
        init_meas = matr(1);
        matr = matr(3:end);
        
        %replace all zeroes of the matrix with a small number, so I can avoid out of bound errors
        %or empty arrays
        t = matr == 0;
        matr(t) = 0.0001;
        
        transform(matr,init_meas,num_ev(j),time);
        %remove the first num_ev(j) events from the time vector so I can
        %start from new time events every iteration
        if (num_ev(j)~=0)
            rows_rem = 1:num_ev(j);
            time(rows_rem,:)=[];
        end
       
        
    end
    j=1;
end
